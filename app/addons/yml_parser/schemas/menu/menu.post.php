<?php

defined('BOOTSTRAP') or die('Access denied');

$schema['top']['administration']['items']['yml_parser'] = [
    'href' => 'yml_parser.manage',
    'position' => 1000,
];

return $schema;
