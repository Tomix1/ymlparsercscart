<?php

namespace Tygh\YmlParser;
use Tygh\Registry;

class RestApiRequest
{
    private $apiKey = '';
    private $email = '';
    public $url;
    public $error;
    public $log;
    public bool $success;


    function __construct()
    {
        $this->email = Registry::get('addons.yml_parser.email');
        $this->apiKey = Registry::get('addons.yml_parser.api_key');
        $this->url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'].'/multivendor_v4.14.1.SP1';
    }

    /**
     * @param string
     * @param array
     *
     * @return array|string
     */
    private function post($resource, $request)
    {
        $url = $this->url . $resource;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$this->email:$this->apiKey");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($ch), true);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ((string)$httpcode === '201') {
            $this->success = true;
        } else { //HTTP/1.1 400 Bad Request
            $this->success = false;
        }
        curl_close($ch);
        $this->log.=$url.' -post '.$httpcode.' - '.var_export($response, true).PHP_EOL;
        return $response;
    }

    /**
     * @param array
     *
     * @return array|string
     */
    public function createProduct($request)
    {
        $resource = '/api/2.0/products/';
        return $this->post($resource, $request);
    }

    /**
     * @param array
     *
     * @return array|string
     */
    public function createCategory($request)
    {
        $resource = '/api/2.0/categories/';
        return $this->post($resource, $request);
    }

    /**
     * @param string
     * @param array
     *
     * @return array|string
     */
    private function put($resource, $request)
    {
        unset($request['product_id']);
        $url = $this->url . $resource;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$this->email:$this->apiKey");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($ch), true);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpcode === '200') {
            $this->success = true;
        } else {
            $this->success = false;
        }
        curl_close($ch);
        $this->log.=$url.' -put '.$httpcode.' - '.var_export($response, true).var_export($request, true).PHP_EOL;
        return $response;
    }

    /**
     * @param array
     *
     * @return array|string
     */
    public function updateProduct($request)
    {
        $resource = '/api/2.0/products/'.$request['product_id'];
        return $this->put($resource, $request);
    }

    public function updateCategory($request)
    {
        $resource = '/api/2.0/categories/'.$request['category_id'];
        return $this->put($resource, $request);
    }

    /**
     * @param string
     *
     * @return array|string
     */
    private function delete($resource)
    {
        $url = $this->url . $resource;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$this->email:$this->apiKey");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($ch), true);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpcode === '204') {
            $this->success = true;
        } else { //HTTP/1.1 400 Bad Request | HTTP/1.1 404 Not Found
            $this->success = false;
        }
        curl_close($ch);
        $this->log.=$url.' - '.$httpcode.' - '.var_export($response, true).PHP_EOL;
        return $response;
    }

    /**
     * @param int
     *
     * @return array|string
     */
    public function deleteProduct($id)
    {
        $resource = '/api/2.0/products/'.$id;
        return $this->delete($resource);
    }

    /**
     * @param int
     *
     * @return array|string
     */
    public function deleteCategory($id)
    {
        $resource = '/api/2.0/categories/'.$id;
        return $this->delete($resource);
    }

    /**
     * @param string
     *
     * @return array|string
     */
    private function get($resource)
    {
        $url = $this->url . $resource;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$this->email:$this->apiKey");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($ch), true);
        curl_close($ch);
        //$this->log.=$url.' - '.var_export($response, true).PHP_EOL;
        return $response;
    }

    /**
     * @return array|string
     */
    public function getProductList()
    {
        $resource = '/api/2.0/products';
        return $this->get($resource);
    }

    /**
     * @return array|string
     */
    public function getCategoryList()
    {
        $resource = '/api/2.0/categories';
        return $this->get($resource);
    }

    /**
     * @param int
     *
     * @return array|string
     */
    public function getProductDetails($id)
    {
        $resource = '/api/2.0/products/'.$id;
        return $this->get($resource);
    }

    /**
     * @param int
     *
     * @return array|string
     */
    public function getCategoryDetails($id)
    {
        $resource = '/api/2.0/categories/'.$id;
        return $this->get($resource);
    }
}
