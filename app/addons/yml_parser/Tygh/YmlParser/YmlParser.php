<?php

namespace Tygh\YmlParser;
use Tygh\YmlParser\RestApiRequest;
use Tygh\Registry;

class YmlParser
{
    public $path;
    public $name;
    public $company;
    public $company_id;
    public $error;
    public $log;
    public $fileError;
    public $fileLog;
    public $statusOrder;
    public $statusCategory;
    private $api;
    private $XMLReader;
    private $categoryList;
    private $offersFlag = 0;

    function __construct($path)
    {
        $this->path = $path;
        $this->XMLReader = new \XMLReader();
        $this->api = new RestApiRequest();
        $this->statusOrder = 'A';//Registry::get('addons.yml_parser.status_order');
        $this->statusCategory = 'A';//Registry::get('addons.yml_parser.status_category');
        $this->company_id = '1';//Registry::get('runtime.company_id');
    }

    public function run()
    {

        if (!$this->XMLReader->open($this->path)) {
            throw new RuntimeException("Could not open {$this->path} with XMLReader");
        }

        $this->categoryList = $this->api->getCategoryList();

        $this->parse();

        //составление отчета о результате импорта


        $file = str_replace(' ', '_', $this->company);
        $file .= '_' . date('Y-m-d_H:i:s');
        //обработка ошибок
        $this->fileError = $file.'_error.csv';
        file_put_contents(fn_get_files_dir_path() . $this->fileError, $this->error, FILE_APPEND);
        if(!empty($this->fileError)) {
            fn_set_session_data('yml_parser_error', $this->fileError);
        }

        $this->log.=$this->api->log;
        //запись логов
        $this->fileLog = $file.'_log.csv';
        file_put_contents(fn_get_files_dir_path() . $this->fileLog, $this->log, FILE_APPEND);
        if(!empty($this->fileLog)) {
            fn_set_session_data('yml_parser_log', $this->fileLog);
        }
    }

    /**
     * @param SimpleXMLElement
     *
     * @return array
     */
    private function xmlToArray($elem, $tags = [])
    {
        $arr = [];
        foreach ($elem->children() as $child) {
            if (!empty($tags)) {
                if (in_array($child->getName(), $tags)) {
                    if ($child->getName() === 'param') {
                        if ($child->attributes()) {
                            foreach ($child->attributes() as $name => $value) {
                                $arr['param'][$value->__toString()] = $child->__toString();
                            }
                        }
                    } else {
                        $arr[$child->getName()] = $child->__toString();
                    }
                }
            } else {
                if ($child->getName() === 'param') {
                    if ($child->attributes()) {
                        foreach ($child->attributes() as $name => $value) {
                            $arr['param'][$value->__toString()] = $child->__toString();
                        }
                    }
                } else {
                    $arr[$child->getName()] = $child->__toString();
                }
            }

        }
        if ($elem->getName() === 'category') {
            $arr[$elem->getName()] = $elem->__toString();
        }
        if ($elem->attributes()) {
            foreach ($elem->attributes() as $name => $value) {
                $arr[$name] = $value->__toString();
            }
        }
        return $arr;
    }

    /**
     * @param string
     *
     * @return string
     */
    private function tagToParams($tag)
    {
        switch ($tag) {
            case 'name':
                return 'product';
            case 'count':
                return 'amount';
            case 'categoryId':
                return 'main_category';
            case 'delivery':
                return 'edp_shipping';
            case 'description':
                return 'full_description';
            case 'downloadable':
                return 'is_edp';
            case 'parentId':
                return 'parent_id';
            default:
                return $tag;
        }
    }

    /**
     * @param string $tag
     * @param string|int $value
     * @return string|int
     */
    private function tagValueConvert($tag, $value)
    {
        switch ($tag) {
            case 'edp_shipping':
            case 'is_edp':
                if ($value === 'true') {
                    $value = 'Y';
                } else {
                    $value = 'N';
                }
                return $value;
            case 'parent_id':
            case 'main_category':
                $value = $this->dbCheckCategoryExist($value, $this->company_id);
                return $value;
            default:
                if (is_string($value)) {
                    str_replace('&quot;', '"', $value);
                    str_replace('&amp;', '&', $value);
                    str_replace('&gt;', '>', $value);
                    str_replace('&lt;', '<', $value);
                    str_replace('&apos;', "'", $value);
                }
                return $value;
        }
    }

    /**
     * @param array
     *
     * @return array
     */
    private function matchTagToParams($arr_tag)
    {
        $arr_params = [];
        foreach ($arr_tag as $tag =>$value){
            $arr_params[$this->tagToParams($tag)] = $this->tagValueConvert($this->tagToParams($tag), $value);
        }
        return $arr_params;
    }


    private function parse()
    {
        while ($this->XMLReader->read()) {
            switch ($this->XMLReader->name) {
                case 'name':
                    $elem = new \SimpleXMLElement($this->XMLReader->readOuterXML());
                    $this->name = $elem->__toString();
                    $this->XMLReader->next();
                    break;
                case 'company':
                    $elem = new \SimpleXMLElement($this->XMLReader->readOuterXML());
                    $this->company = $elem->__toString();
                    $this->XMLReader->next();
                    break;
                case 'category':
                    $elem = new \SimpleXMLElement($this->XMLReader->readOuterXML());
                    $category_array = $this->xmlToArray($elem);
                    $category_array = $this->matchTagToParams($category_array);

                    foreach ($this->categoryList['categories'] as $number => $category){
                        if ((string)$category['category'] === (string)$category_array['category']){
                            $category_array['category_id'] = $category['category_id'];
                            if (!empty($category['parent_id'])) {
                                $category_array['parent_id'] = $category['parent_id'];
                            }
                            break;
                        }
                    }
                    $category_array['status'] = $this->statusCategory;
                    $category_array['company_id'] = $this->company_id;
                    if (empty($category_array['category_id'])) {
                        $category_array['category_id'] = $this->dbCheckCategoryExist($category_array['categoryId'], $this->company_id);
                    }
                    $params = ['category', 'company_id', 'status'];
                    foreach ($params as $param) {
                        if (!isset($category_array[$param])) {
                            $this->error .= "Error: $param is not exist of category ". $category_array['id'] . PHP_EOL;
                        }
                    }

                    if ($category_array['category_id'] === false) {
                        $response = $this->api->createCategory($category_array);
                        if ($this->api->success) {
                            $this->dbLinkCategory($category_array['category_id'], $category_array['id'], $this->company_id);
                            $this->log.=$response['category_id'].' '.$response['category'].' - '.'Created'.PHP_EOL;
                        } else {
                            $this->log.=$category_array['category_id'].' - '.'Error'.PHP_EOL;
                            $this->log.='categoryList  - '.var_export($category_array, true).PHP_EOL;
                        }
                    } else {
                        $response = $this->api->updateCategory($category_array);
                        if ($this->api->success) {
                            $this->dbLinkCategory($category_array['category_id'], $category_array['id'], $this->company_id);
                            $this->log.=$response['category_id'].' '.$category_array['category'].' - '.'Updated'.PHP_EOL;
                        } else {
                            $this->log.=$category_array['category_id'].' - '.'Error'.PHP_EOL;
                        }
                        //$this->dbLinkCategory($category_array['category_id'], $category_array['id'], $this->company_id);
                    }
                    $this->XMLReader->next();
                    break;
                case 'offers':
                    if ($this->offersFlag === 0) {
                        $this->offersFlag=1;
                    } else {
                        break 2;
                    }
                    //$this->categoryList = $this->api->getCategoryList();

                    break;
                case 'offer':
                    $elem = new \SimpleXMLElement($this->XMLReader->readOuterXML());
                    $tags = [
                        'name',
                        //'company',
                        //'url',
                        'typePrefix',
                        'vendor',
                        'model',
                        'price',
                        //'oldprice',
                        //'purchase_price',
                        'categoryId',
                        'picture', //main pair
                        //'supplier',
                        'delivery',
                        //'pickup',
                        //'store',
                        'description',
                        //'barcode',
                        //'vat',
                        //'expiry',
                        'weight',
                        'dimensions',
                        //'downloadable',
                        'count',
                        'param'
                    ];
                    $offer_array = $this->xmlToArray($elem, $tags);

                    if (isset($offer_array['dimensions'])) {
                        $arr = explode('/', $offer_array['dimensions']);
                        $offer_array['length'] = $arr[0];
                        $offer_array['width'] = $arr[1];
                        $offer_array['height'] = $arr[2];
                        unset($offer_array['dimensions']);
                    }
                    if (isset($offer_array['typePrefix'])) {
                        $offer_array['product'] = $offer_array['typePrefix'] .' '. $offer_array['vendor'] .' '. $offer_array['model'];
                        unset($offer_array['typePrefix']);
                        unset($offer_array['vendor']);
                        unset($offer_array['model']);
                    }
                    $offer_array = $this->matchTagToParams($offer_array);
                    $offer_array['status'] = $this->statusOrder;
                    $offer_array['company_id'] = $this->company_id;
                    $offer_array['category_ids'] = $offer_array['main_category'];

                    $params = ['product', 'category_ids', 'main_category', 'price', 'company_id', 'status'];
                    foreach ($params as $param) {
                        if (!isset($offer_array[$param])) {
                            $this->error.= "Error: $param is not exist of offer ". $offer_array['id'] . PHP_EOL;
                        }
                    }
                    $product_id = $this->dbCheckOfferExist($offer_array['id'], $this->company_id);

                    if ($product_id === false) {
                        $response = $this->api->createProduct($offer_array);
                        if ($this->api->success) {
                            $this->dbLinkOffer($response['product_id'], $offer_array['id'], $this->company_id);
                            $this->log.=$response['product_id'].' '.$response['product'].' - '.'Created'.PHP_EOL;
                        } else {
                            $this->log.=$offer_array['category_id'].' - '.'Error Created'.PHP_EOL;
                        }
                    } else {
                        //$offer_array1 = $this->api->getProductDetails($product_id);
                        $offer_array['product_id'] = $product_id;
                        $offer_array['status'] = 'D';
                        $response = $this->api->updateProduct($offer_array);
                        if ($this->api->success) {
                            $this->dbLinkOffer($offer_array['product_id'], $offer_array['id'], $this->company_id);
                            $this->log.=$response['product_id'].' '.$offer_array['product'].' - '.'Updated'.PHP_EOL;
                        } else {
                            $this->log.=$offer_array['id'].' - '.'Error Updated'.PHP_EOL;
                        }
                    }
                    $this->XMLReader->next();
                    break;
            }
        }
    }

    private function dbLinkCategory($category_id, $categoryId, $company_id)
    {
        $result = db_query('REPLACE INTO ?:yml_parser_linking_category (category_id, categoryId, company_id) VALUES (?i, ?i, ?i)', $category_id, $categoryId, $company_id);
    }

    private function dbLinkOffer($product_id, $offer_id, $company_id)
    {
        $result = db_query('REPLACE INTO ?:yml_parser_linking_product_offer (product_id, offer_id, company_id) VALUES (?i, ?i, ?i)', $product_id, $offer_id, $company_id);
    }

    private function dbCheckCategoryExist($categoryId, $company_id)
    {
        $result = db_get_field('SELECT category_id FROM ?:yml_parser_linking_category WHERE categoryId = ?i AND company_id = ?i', $categoryId, $company_id);
        if ($result === null || $result === '') {
            $result = false;
        }
        return $result;
    }

    private function dbCheckOfferExist($offer_id, $company_id)
    {
        $result = db_get_field('SELECT product_id FROM ?:yml_parser_linking_product_offer WHERE offer_id = ?i AND company_id = ?s', $offer_id, $company_id);
        if ($result === null || $result === '') {
            $result = false;
        }
        return $result;
    }

    private function dbDeleteLinkCategory($category_id, $company_id)
    {
        $result = db_query('DELETE FROM ?:yml_parser_linking_category WHERE category_id = ?i AND company_id = ?i', $category_id, $company_id);
    }

    private function dbDeleteLinkOffer($product_id)
    {
        $result = db_query('DELETE FROM ?:yml_parser_linking_product_offer WHERE product_id = ?i', $product_id);
    }
}
