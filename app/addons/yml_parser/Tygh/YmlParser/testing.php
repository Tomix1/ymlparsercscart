<?php

$offers = 0;
$categoryList = array (
        'categories' =>
            array (
                0 =>
                    array (
                        'category_id' => '7',
                        'parent_id' => '0',
                        'id_path' => '7',
                        'category' => 'Сэндвичницы и приборы для выпечки',
                        'position' => '0',
                        'status' => 'A',
                        'company_id' => '1',
                        'storefront_id' => '0',
                        'product_count' => '0',
                        'seo_name' => NULL,
                        'seo_path' => NULL,
                    ),
                1 =>
                    array (
                        'category_id' => '6',
                        'parent_id' => '0',
                        'id_path' => '6',
                        'category' => 'Мелкая техника для кухни',
                        'position' => '0',
                        'status' => 'A',
                        'company_id' => '1',
                        'storefront_id' => '0',
                        'product_count' => '0',
                        'seo_name' => NULL,
                        'seo_path' => NULL,
                    ),
                2 =>
                    array (
                        'category_id' => '5',
                        'parent_id' => '0',
                        'id_path' => '5',
                        'category' => 'Бытовая техника',
                        'position' => '0',
                        'status' => 'A',
                        'company_id' => '1',
                        'storefront_id' => '0',
                        'product_count' => '0',
                        'seo_name' => NULL,
                        'seo_path' => NULL,
                    ),
                3 =>
                    array (
                        'category_id' => '4',
                        'parent_id' => '0',
                        'id_path' => '4',
                        'category' => 'Сэндвичницы и приборы для выпечки',
                        'position' => '0',
                        'status' => 'A',
                        'company_id' => '1',
                        'storefront_id' => '0',
                        'product_count' => '0',
                        'seo_name' => NULL,
                        'seo_path' => NULL,
                    ),
                4 =>
                    array (
                        'category_id' => '3',
                        'parent_id' => '0',
                        'id_path' => '3',
                        'category' => 'Мелкая техника для кухни',
                        'position' => '0',
                        'status' => 'A',
                        'company_id' => '1',
                        'storefront_id' => '0',
                        'product_count' => '0',
                        'seo_name' => NULL,
                        'seo_path' => NULL,
                    ),
                5 =>
                    array (
                        'category_id' => '2',
                        'parent_id' => '0',
                        'id_path' => '2',
                        'category' => 'Бытовая техника',
                        'position' => '0',
                        'status' => 'A',
                        'company_id' => '1',
                        'storefront_id' => '0',
                        'product_count' => '0',
                        'seo_name' => NULL,
                        'seo_path' => NULL,
                    ),
            ),
        'params' =>
            array (
                'category_id' => 0,
                'visible' => false,
                'current_category_id' => 0,
                'simple' => false,
                'plain' => true,
                'limit' => 0,
                'item_ids' => '',
                'group_by_level' => false,
                'get_images' => false,
                'category_delimiter' => '/',
                'get_frontend_urls' => false,
                'max_nesting_level' => NULL,
                'get_company_name' => false,
                'storefront_ids' => NULL,
                'items_per_page' => 50,
                'page' => 1,
                'except_id' => 1,
                'sort_order' => 'asc',
                'sort_by' => 'position',
                'sort_order_rev' => 'desc',
                'total_items' => '6',
            ),
    );

/**
 * @param SimpleXMLElement
 *
 * @return array
 */
function xmlToArray($elem, $tags = []){
    $arr = [];
    foreach ($elem->children() as $child) {
        if (!empty($tags)) {
            if (in_array($child->getName(), $tags)) {
                if ($child->getName() === 'param') {
                    if ($child->attributes()) {
                        foreach ($child->attributes() as $name => $value) {
                            $arr['param'][$value->__toString()] = $child->__toString();
                        }
                    }
                } else {
                    $arr[$child->getName()] = $child->__toString();
                }
            }
        } else {
            if ($child->getName() === 'param') {
                if ($child->attributes()) {
                    foreach ($child->attributes() as $name => $value) {
                        $arr['param'][$value->__toString()] = $child->__toString();
                    }
                }
            } else {
                $arr[$child->getName()] = $child->__toString();
            }
        }

    }
    if ($elem->getName() === 'category') {
        $arr[$elem->getName()] = $elem->__toString();
    }
    if ($elem->attributes()) {
        foreach ($elem->attributes() as $name => $value) {
            $arr[$name] = $value->__toString();
        }
    }
    return $arr;
}

function tagValueConvert($tag, $value)
{
    switch ($tag) {
        case 'edp_shipping':
        case 'is_edp':
            if ($value === 'true') {
                $value = 'Y';
            } else {
                $value = 'N';
            }
            return $value;
        case 'parentId':
            $value = dbCheckCategoryExist($value, 'company_id');
            return $value;
        case 'main_category':
            $value = dbCheckCategoryExist($value, 'company_id');
            return $value;
        default:
            if (is_string($value)) {
                str_replace('&quot;', '"', $value);
                str_replace('&amp;', '&', $value);
                str_replace('&gt;', '>', $value);
                str_replace('&lt;', '<', $value);
                str_replace('&apos;', "'", $value);
            }
            return $value;
    }
}

function tagToParams($tag){
    $match = [
        'categoryId' => 'main_category',
        //'status' => A for Active, D for Disabled, H for Hidden
        'description' => 'full_description',
        //'description' => 'meta_description',
        //'description' => 'short_description',
    ];
    switch ($tag) {
        case 'name':
            return 'product';
        case 'count':
            return 'amount';
        case 'categoryId':
            return 'main_category';
        case 'delivery':
            return 'edp_shipping';
        case 'description':
            return 'full_description';
        case 'downloadable':
            return 'is_edp';
        default:
            return $tag;
    }
}

function matchTagToParams($arr_tag){
    $arr_params = [];
    foreach ($arr_tag as $tag =>$value){
        $arr_params[tagToParams($tag)] = tagValueConvert(tagToParams($tag), $value);
    }
    return $arr_params;
}


function parser($file){
    $reader = new XMLReader();
    if (!$reader->open($file)) {
        throw new RuntimeException("Could not open {$file} with XMLReader");
    }
    while ($reader->read()) {
        switch ($reader->name) {
            case 'name':
                $elem = new SimpleXMLElement($reader->readOuterXML());
                $name = $elem->__toString();
                echo 'name'.$name.PHP_EOL;
                $reader->next();
                break;
            case 'company':
                $elem = new SimpleXMLElement($reader->readOuterXML());
                $company = $elem->__toString();
                echo 'company'.$company.PHP_EOL;
                $reader->next();
                break;
            case 'category':
                $elem = new SimpleXMLElement($reader->readOuterXML());
                $category_array = xmlToArray($elem);
                $category_array = matchTagToParams($category_array);
                $category_array['company_id'] = 'company_id';
                $category_array['status'] = 'status';

                $params = ['category', 'company_id', 'status'];
                foreach ($params as $param) {
                    if (!isset($category_array[$param])) {
                        echo "Error: $param is not exist of category ". $category_array['id'] . PHP_EOL;
                    }
                }
                //api create category


                print_r($category_array);

                $reader->next();
                break;
            case 'offers':
                //Когда парсер дойдёт до тега offers, новые категории товаров будут уже добавлены
                //Здесь нужно подготовить category_id для($offer_array) добавления товаров из новых категорий
                global $offers;
                if ($offers === 0) {
                    $offers++;
                } else {
                    break 2;
                }
                global $categoryList;
                foreach ($categoryList['categories'] as $number => $category){
                    echo $category['category_id'].PHP_EOL;
                }
                echo 'offers'.PHP_EOL;



                break;
            case 'offer':
                $elem = new SimpleXMLElement($reader->readOuterXML());
                $tags = [
                    'name',
                    //'company',
                    //'url',
                    'typePrefix',
                    'vendor',
                    'model',
                    'price',
                    //'oldprice',
                    //'purchase_price',
                    'categoryId',
                    'picture',
                    //'supplier',
                    'delivery',
                    //'pickup',
                    //'store',
                    'description',
                    //'barcode',
                    //'vat',
                    //'expiry',
                    'weight',
                    'dimensions',
                    //'downloadable',
                    'count',
                    'param'
                ];
                $offer_array = xmlToArray($elem, $tags);
                echo 'offer';


                if (isset($offer_array['dimensions'])) {
                    $arr = explode('/', $offer_array['dimensions']);
                    $offer_array['length'] = $arr[0];
                    $offer_array['width'] = $arr[1];
                    $offer_array['height'] = $arr[2];
                    unset($offer_array['dimensions']);
                }
                if (isset($offer_array['typePrefix'])) {
                    $offer_array['product'] = $offer_array['typePrefix'] .' '. $offer_array['vendor'] .' '. $offer_array['model'];
                    unset($offer_array['typePrefix']);
                    unset($offer_array['vendor']);
                    unset($offer_array['model']);
                }
                $offer_array = matchTagToParams($offer_array);
                $offer_array['company_id'] = 'company_id';
                $offer_array['status'] = 'status';

                $offer_array['main_category'] = dbCheckCategoryExist($offer_array['main_category'], $offer_array['company_id']);
                $offer_array['category_ids'] = $offer_array['main_category'];

                $params = ['product', 'category_ids', 'main_category', 'price', 'company_id', 'status'];
                foreach ($params as $param) {
                    if (!isset($offer_array[$param])) {
                        echo "Error: $param is not exist of category ". $offer_array['id'] . PHP_EOL;
                    }
                }

                print_r($offer_array);


                $reader->next();
                break;
        }
    }
}

function dbCheckCategoryExist($categoryId, $company_id){
    return 'category_id'.$categoryId.'company_id';
}

$file = 'YML_sample.xml';
$tag = 'offer';
parser($file);