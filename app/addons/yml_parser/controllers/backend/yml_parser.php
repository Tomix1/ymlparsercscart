<?php

use Tygh\YmlParser\YmlParser;
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']	=== 'POST') {

    if ($mode === 'import') {
        $file = fn_filter_uploaded_data('yml_file');
        if (!empty($file)) {
            $path = $file[0]['path'];
            $YmlParser = new YmlParser($path);
            $YmlParser->run();
            if (!empty($YmlParser->fileError)) {
                fn_set_notification('W', __('important'), __('text_exim_data_imported', array(
                    '[error]' => 'См. файл'.$YmlParser->fileError,
                )));
            }
            if (!empty($YmlParser->fileLog)) {
                fn_set_notification('W', __('important'), __('text_exim_data_imported', array(
                    '[log]' => 'См. файл'.$YmlParser->fileLog,
                )));
            }
        } else {
            fn_set_notification('E', __('error'), __('error_exim_no_file_uploaded'));
        }
    }

    $suffix = ".manage";

    return array(CONTROLLER_STATUS_OK, "yml_parser$suffix");
}

if ($mode === 'manage') {

    $tabs = array (
        'import' => array (
            'title' => __('general'),
            'js' => true
        ),
        'import_result' => array (
            'title' => __('yml_parser.import_result'),
            'js' => true
        ),
    );
    Registry::set('navigation.tabs', $tabs);
    $log['log'] = fn_get_session_data('yml_parser_log');
    $log['errors'] = fn_get_session_data('yml_parser_errors');
    if (!empty($log)) {
        Tygh::$app['view']->assign('log', $log);
    }
}
