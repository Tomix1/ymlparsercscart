{capture name="mainbox"}

<form action="{""|fn_url}" method="post" class="form-horizontal form-edit " name="yml_parser_form" enctype="multipart/form-data">
<input type="hidden" class="cm-no-hide-input" name="fake" value="1" />

{capture name="tabsbox"}

    {include file="common/subheader.tpl" title=__("yml_parser.import") target="#import"}
    <div id="import" class="in collapse">
        <div class="control-group">
            <label class="control-label">{__("select_file")}:</label>
            <div class="controls">{include file="common/fileuploader.tpl" var_name="yml_file[0]"}</div>
        </div>

    </div>

{/capture}

{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}
{capture name="buttons"}
    {include file="buttons/button.tpl" but_text=__("import") but_name="dispatch[yml_parser.import]" but_role="submit-link" but_target_form="yml_parser_form" but_meta="cm-tab-tools"}
{/capture}

</form>

{/capture}

{include file="common/mainbox.tpl" title=__('yml_parser.title') content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}